# Fab Academy

This is a documentation website for Digital Fabrication 2023.

## Usage

Clone the repository and edit it according to your needs.

## More

You can learn more about the Fab Academy on [this 
website](https://fabacademy.org/).

